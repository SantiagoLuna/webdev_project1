/**
 * @author Santiago Luna & Maria Maria Ramirez
 * This script adds functionality to the spending log site. This script
 * allows the user to dynamically add entries to the spending log with infomation
 * such as date, a description of the purpose, the amount of money the purchase was, 
 * and what kind of purchase it was (School, Entertainment, Food, or other). 
 * Once this information is submitted, it is used to populate a JSON object to then
 * be pushed into an array and then being sent to the localstorage.
 */

"use strict";

window.addEventListener("DOMContentLoaded", setup);

/**
 * @author Santiago Luna & Maria Maria Ramirez
 * adding event listener to submit button and creating array in local storage
 */
function setup(){
    let form = document.querySelector('#form');
    form.addEventListener('submit', populate);
    if(localStorage.getItem('formValues')){
        loadSpending();
    }
    

    let clearBtn = document.querySelector('#clear');
    clearBtn.addEventListener('click', function() {localStorage.clear();}); 
}

let arr = [];
let count = 0;

/**
 * @author Santiago Luna & Maria Maria Ramirez
 * creates JSON objects using inputs submitted by client, pushes them into array,
 * and sends them to local storage
 */
function populate(e){
    e.preventDefault();

   let date = document.querySelector('#date').value;
   let description = document.querySelector('#desc').value;
   let amount = document.querySelector('#amount').value;
   let category = document.querySelector('input[type = radio]:checked').value;
   
    console.log('populating...');

    //creating JSON obj
    let values = {
        'uniqueID': arr.length,
        'date': date,
        'description': description,
        'amountSpent': amount,
        'category': category
    };
    //pushing the obj into the array then sending it to localstorage
    arr.push(values);
    localStorage.setItem('formValues', JSON.stringify(arr));
    createEntry(values);
}

/**
 * @author Santiago Luna & Maria Maria Ramirez
 * initializing array
 */
function loadSpending(){
    arr = JSON.parse(localStorage.getItem('formValues'));

    arr.forEach(x => {
        createEntry(x);
    });
}

/**
 * @author Santiago Luna & Maria Maria Ramirez
 * creates new div for each entry submitted by client. 
 * Evaluates how many stars each entry should be assigned (1 for > $100, 2 for > $500, 3 for > 750)
 * Creates checkbox to delete the entries when desired
 * Adds 
 */
function createEntry(obj){

    //create new div for the new entry
    let entries = document.querySelector('#entries');
    let newEntry = document.createElement('div');
    newEntry.setAttribute('id', count);
    entries.appendChild(newEntry);

    //create entry category
    let category = document.createElement('h4');

    if (obj.amountSpent >= 100 && obj.amountSpent < 500){
        category.textContent = obj.category + "   " + '\u2606';
        newEntry.appendChild(category);
        newEntry.style.backgroundColor = '#D291BC';
    }

    else if (obj.amountSpent >= 500 && obj.amountSpent < 750){
        category.textContent = obj.category + "   " + '\u2606' + '\u2606';
        newEntry.appendChild(category);
        newEntry.style.backgroundColor = "#957DAD";
    }

    else if (obj.amountSpent >= 750){
        category.textContent = obj.category + "   " + '\u2606' + '\u2606' + '\u2606';
        newEntry.appendChild(category);
        newEntry.style.backgroundColor = "#E0BBE4";
    }
    else{
        category.textContent = obj.category;
        newEntry.appendChild(category);
    }

    //create checkbox
    let checkbox = document.createElement('input');
    checkbox.setAttribute('type', 'checkbox');
    checkbox.setAttribute('id', 'checkbox');
    newEntry.appendChild(checkbox);

    //create entry description 
    let description = document.createElement('p');
    description.textContent = obj.description;
    newEntry.appendChild(description);

    //create entry date
    let date = document.createElement('p');
    date.textContent = obj.date;
    newEntry.appendChild(date);

    //create entry price
    let price = document.createElement('p');
    price.innerHTML = "Price: $" + obj.amountSpent;
    newEntry.appendChild(price);

    //create unique id
    let id = document.createElement('p');
    id.textContent = `unique ID: ${obj.uniqueID}`;
    newEntry.appendChild(id);

    checkbox.addEventListener('change', checkedAction);

    count++;
}

//deleting entries from DOM/array/local storage
function checkedAction(e){
    let divID = e.target.parentNode.getAttribute('id');
    console.log('checked');
    e.currentTarget.parentNode.remove();
    arr.splice(divID, 1);
    
    let divs = document.querySelectorAll('div');
    for(let i=0; i<divs.length; i++){
        divs[i].id = i;
    }
    arr.forEach((entry, index) => entry.uniqueID = index);

    localStorage.setItem('formValues', JSON.stringify(arr));
    count--;
}